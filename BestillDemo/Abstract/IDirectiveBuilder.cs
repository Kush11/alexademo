﻿using Alexa.NET.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Abstract
{
    public interface IDirectiveBuilder
    {
        Task<RenderDocumentDirective> BuildTextDirective(string sentence, string bg = "");
        Task<RenderDocumentDirective> BuildDefaultDirective(string sentences, string title = "", string subtitle = "");

    }
}
