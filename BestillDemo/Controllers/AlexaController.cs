﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BestillDemo.Controllers
{
    [Route("api/[controller]")]
    public class AlexaController : Controller
    {
        private readonly IAddPrayerIntentHandler _addPrayerIntentHandler;
        private readonly IClearPrayerIntentHandler _clearPrayerIntentHandler;
        private readonly IViewPrayerIntentHandler _viewPrayerIntentHandler;
        private readonly ISavePrayerIntentHandler _savePrayerIntentHandler;
        private readonly ILaunchRequestHandler _launchRequest;
        private readonly IMenuIntentHandler _menuIntentHandler;
        private readonly IYesIntentHandler _yesIntentHandler;
        private readonly INoIntentHandler _noIntentHandler;


        private readonly IArchivePrayerIntentHandler _archivePrayerIntentHandler;
        readonly ILogger<AlexaController> _log;
        private readonly IReminderIntentHandler _reminderIntentHandler;


        public AlexaController(INoIntentHandler noIntentHandler, IYesIntentHandler yesIntentHandler, IMenuIntentHandler menuIntentHandler, IClearPrayerIntentHandler clearPrayerIntentHandler, IAddPrayerIntentHandler addPrayerIntentHandler, IViewPrayerIntentHandler viewPrayerIntentHandler, ISavePrayerIntentHandler savePrayerIntentHandler, ILaunchRequestHandler launchRequest, IArchivePrayerIntentHandler archivePrayerIntentHandler, ILogger<AlexaController> log, IReminderIntentHandler reminderIntentHandler)
        {
            _addPrayerIntentHandler = addPrayerIntentHandler;
            _clearPrayerIntentHandler = clearPrayerIntentHandler;
            _viewPrayerIntentHandler = viewPrayerIntentHandler;
            _savePrayerIntentHandler = savePrayerIntentHandler;
            _launchRequest = launchRequest;
            _archivePrayerIntentHandler = archivePrayerIntentHandler;
            _log = log;
            _reminderIntentHandler = reminderIntentHandler;
            _menuIntentHandler = menuIntentHandler;
            _noIntentHandler = noIntentHandler;
            _yesIntentHandler = yesIntentHandler;

        }

        [HttpPost]
        public SkillResponse bestil([FromBody]SkillRequest input)
        {


            Session session = input.Session;
            SkillResponse response = ResponseBuilder.Empty();
            var isAudio = !input.Context.System.Device.IsInterfaceSupported("Display");



            if (session.Attributes == null)
            {
                session.Attributes = new Dictionary<string, object>();

            }

            if (input.GetRequestType() == typeof(LaunchRequest))
            {
                _log.LogInformation("Hello, world!");
                response = _launchRequest.Launch(session).Result;
                //response.Response.Directives.Add(_builder.BuildTextDirective("welcome to the prayer app").Result);
            }
            else if (input.GetRequestType() == typeof(IntentRequest))
            {
                var intentRequest = (IntentRequest)input.Request;
                switch (intentRequest.Intent.Name)
                {
                    case "AddPrayerIntent":
                        response = _addPrayerIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "ClearPrayerIntent":
                        response = _clearPrayerIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "SavePrayerIntent":
                        response = _savePrayerIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "ArchivePrayerIntent":
                        response = _archivePrayerIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "ViewPrayerIntent":
                        response = _viewPrayerIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "ReminderIntent":
                        response = _reminderIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "MenuIntent":
                        response = _menuIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "AMAZON.NoIntent":
                        response = _noIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                    case "AMAZON.YesIntent":
                        response = _yesIntentHandler.HandleIntent(intentRequest, session).Result;
                        break;
                }
            }

            return response;
        }

    }
}