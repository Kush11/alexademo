﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using BestillDemo.Directives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class ViewPrayerIntentHandler : IViewPrayerIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest input, Session session)
        {
            var sessionAttr = session.Attributes["prayer_request"];
            string pry_msg;
            if (sessionAttr == null)
            {
                pry_msg = "You have no active prayer requests";
            }
            else
            {
                pry_msg = "your prayer request is : prayer for " + (string)session.Attributes["prayer_request"];
            }


            Reprompt rp = new Reprompt(pry_msg);
            var response = ResponseBuilder.Ask(pry_msg, rp, session);
            var rd = new RenderDirective();
            response.Response.Directives.Add(rd.BuildDefaultDirective(pry_msg, "Your Prayer Request").Result);
            return Task.FromResult(response);
        }
    }
}
