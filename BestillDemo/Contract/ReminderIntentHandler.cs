﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.Directive;
using Alexa.NET.Response.Ssml;
using BestillDemo.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class ReminderIntentHandler : IReminderIntentHandler
    {


       
        public Task<SkillResponse> HandleIntent( IntentRequest input, Session session)
        {
            string itm = input.Intent.Slots["frequency"].Value;
            string start = input.Intent.Slots["starttime"].Value;
            string end = input.Intent.Slots["endtime"].Value;
            var msg = "";
            var updatedIntent = input.Intent;
            if(input.DialogState == "STARTED")
            {
                var response = ResponseBuilder.DialogDelegate(updatedIntent);
                return Task.FromResult(response);

            }
            else if(input.DialogState != "COMPLETED")
            {
                if(itm != null && itm == "once" || itm == "one" || itm == "1")
                {
                    
                    

                    if (start != null)
                    {
                        PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
                        speech.Text = $"Your reminder has been set to start at {start}. You will be reminded {itm}";
                        var response = ResponseBuilder.DialogElicitSlot(speech, "starttime", updatedIntent);
                        return Task.FromResult(response);

                    }
                    var del = ResponseBuilder.DialogDelegate(updatedIntent);
                    return Task.FromResult(del);

                }
                else
                {
                    var response = ResponseBuilder.DialogDelegate(updatedIntent);
                    return Task.FromResult(response);
                }
            }

             msg = $"Your reminder has been set to start at {start} and will end at {end}. You will be reminded {itm}";

            Reprompt rp = new Reprompt(msg);
            var res = ResponseBuilder.Ask(msg, rp, session);

            return Task.FromResult(res);


        }




    }
}
