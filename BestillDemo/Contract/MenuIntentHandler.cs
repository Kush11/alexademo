﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class MenuIntentHandler : IMenuIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest request, Session session)
        {
            string option = request.Intent.Slots["option"].Value;

            //var msg = "";
            var updatedIntent = request.Intent;

            if (string.IsNullOrEmpty(option))
            {
                //msg = "I didn't get that. Please ask again.";
            }
            else
            {
                if (option == "yes")
                {
                    updatedIntent.Name = "ViewPrayerIntent";
                    updatedIntent.ConfirmationStatus = "None";
                    
                }
                else
                {
                    updatedIntent.Name = "AddPrayerIntent";
                    updatedIntent.ConfirmationStatus = "None";
                }

            }

            var res = ResponseBuilder.DialogDelegate(session, updatedIntent);
            return Task.FromResult(res);
        }
    }
}
