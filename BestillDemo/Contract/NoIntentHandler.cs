﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class NoIntentHandler : INoIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest input, Session session)
        {
            //var updatedIntent = input.Intent;
            //updatedIntent.Name = "AddPrayerIntent";
            //updatedIntent.ConfirmationStatus = "None";
            //var res = ResponseBuilder.DialogDelegate(updatedIntent);
            //return Task.FromResult(res);

            string pry_msg;
            var msg = "Do you want me to read out your prayer list?";

            if (session.Attributes.ContainsKey("lastIntent"))
            {
               
                if (session.Attributes["lastIntent"].ToString() == "AddPrayerIntent")
                {
                    pry_msg = $"Do you want to archive a prayer request?";
                    session.Attributes.Remove("lastIntent");
                    session.Attributes["lastIntent"] = "ArchivePrayerIntent";

                    Reprompt rp = new Reprompt(pry_msg);
                    var res = ResponseBuilder.Ask(pry_msg, rp, session);
                    return Task.FromResult(res);
                }

                if (session.Attributes["lastIntent"].ToString().Equals("ArchivePrayerIntent"))
                {
                    pry_msg = $"Do you want to delete a prayer request?";
                    session.Attributes.Remove("lastIntent");
                    session.Attributes["lastIntent"] = "DeletePrayerIntent";

                    Reprompt rp = new Reprompt(pry_msg);
                    var res = ResponseBuilder.Ask(pry_msg, rp, session);
                    return Task.FromResult(res);
                }
            }
            else
            {
                pry_msg = $"Do you want to add a prayer request?";
                session.Attributes["lastIntent"] = "AddPrayerIntent";
                Reprompt rp = new Reprompt(pry_msg);
                var res = ResponseBuilder.Ask(pry_msg, rp, session);
                return Task.FromResult(res);
            }



            Reprompt rep = new Reprompt(msg);
            var response = ResponseBuilder.Ask(msg, rep, session);
            session.Attributes.Remove("lastIntent");
            return Task.FromResult(response);
        }
    }
}
