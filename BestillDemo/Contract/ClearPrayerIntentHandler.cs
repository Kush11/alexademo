﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using BestillDemo.Directives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class ClearPrayerIntentHandler : IClearPrayerIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest input, Session session)
        {
            var sessionAttr = session.Attributes["prayer_request"];
            var pry_msg = "";
            if (sessionAttr == null)
            {
                pry_msg = "You have no active prayer point to clear";
            }
            else
            {
                session.Attributes["prayer_request"] = null;
                pry_msg = "prayer requests has been cleared!";
            }
            Reprompt rp = new Reprompt(pry_msg);
            var response = ResponseBuilder.Ask(pry_msg, rp, session);
            var rd = new RenderDirective();
            response.Response.Directives.Add(rd.BuildTextDirective(pry_msg, "").Result);
            return Task.FromResult(response);
        }
    }
}
