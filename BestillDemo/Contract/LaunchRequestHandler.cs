﻿using Alexa.NET;
using Alexa.NET.APL;
using Alexa.NET.APL.Components;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using Alexa.NET.Response.APL;
using BestillDemo.Abstract;
using BestillDemo.Directives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class LaunchRequestHandler : ILaunchRequestHandler
    {
        public Task<SkillResponse> Launch(Session session)
        {
            string msg = $"Welcome to the Prayer Demo app. Would you like me to read your prayer list?";

            //PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
            //speech.Text = msg;
            Reprompt rp = new Reprompt(msg);
            var res = ResponseBuilder.Ask(msg, rp, session);
            //var up = new IntentRequest().Intent;
            //up.Name = "MenuIntent";
            //var res = ResponseBuilder.DialogDelegate(up);


            var rd = new RenderDirective();
            res.Response.Directives.Add(rd.BuildDefaultDirective(msg, "Welcome").Result);
      


            return Task.FromResult(res);
        }
    }
}
