﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class YesIntentHandler : IYesIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest input, Session session)
        {
            var updatedIntent = input.Intent;
            var sessionAttr = session.Attributes["prayer_request"];


            //if (session.Attributes == null)
            //{
            //    session.Attributes = new Dictionary<string, object>();
            //}
            session.Attributes.Add("prayer_requests", sessionAttr);

            updatedIntent.Name = "ViewPrayerIntent";
            updatedIntent.ConfirmationStatus = "None";
            var res = ResponseBuilder.DialogDelegate(updatedIntent);
            return Task.FromResult(res);
        }
    }
}
