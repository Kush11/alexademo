﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using BestillDemo.Directives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class SavePrayerIntentHandler : ISavePrayerIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest input, Session session)
        {
            string itm = input.Intent.Slots["request"].Value;
            string msg = "";
            if (string.IsNullOrEmpty(itm))
            {
                msg = "I didn't get that. Would you like to ask again.";
                Reprompt er = new Reprompt(msg);
                var resp = ResponseBuilder.Ask(msg, er, session);
                return Task.FromResult(resp);
            }
            session.Attributes["prayer_request"] = itm;
            msg = $"prayer for {itm} has been saved successfully. May God answer your prayers";

            Reprompt rp = new Reprompt(msg);
            var response = ResponseBuilder.Ask(msg, rp, session);
            var rd = new RenderDirective();
            response.Response.Directives.Add(rd.BuildDefaultDirective(msg, "Let's Pray").Result);

            return Task.FromResult(response);
        }
    }
}
