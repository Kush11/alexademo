﻿using Alexa.NET;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;
using Alexa.NET.Response;
using BestillDemo.Abstract;
using BestillDemo.Directives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Contract
{
    public class ArchivePrayerIntentHandler : IArchivePrayerIntentHandler
    {
        public Task<SkillResponse> HandleIntent(IntentRequest input, Session session)
        {
            string msg = $"Your prayer request has been archived successfully";
            Reprompt rp = new Reprompt(msg);
            var response = ResponseBuilder.Ask(msg, rp, session);
            var rd = new RenderDirective();
            response.Response.Directives.Add(rd.BuildTextDirective(msg, "").Result);
            return Task.FromResult(response);
        }
    }
}
