﻿using Alexa.NET;
using Alexa.NET.APL;
using Alexa.NET.APL.Components;
using Alexa.NET.APL.DataSources;
using Alexa.NET.Request;
using Alexa.NET.Response;
using Alexa.NET.Response.APL;
using Alexa.NET.Response.Ssml;
using BestillDemo.Abstract;
using BestillDemo.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestillDemo.Directives
{
    public class RenderDirective : HelperObj, IDirectiveBuilder
    {
        public Task<RenderDocumentDirective> BuildTextDirective(string sentence, string bg = "")
        {
            if (string.IsNullOrEmpty(bg))
            {
                bg = new APLValue<string>(BgUrl);
            }
            var mainLayout = new Layout(
                new Container(
                    new Image(bg)
                    {
                        Width = "100vw",
                        Height = "100vh",
                        Position = new APLValue<string>("absolute"),
                        Scale = new APLValue<Scale?>(Scale.BestFill)
                    },
                    new Text(sentence)
                    {
                        FontSize = "40dp",
                        Style = new APLValue<string>("textStyleBody"),
                        TextAlign = new APLValue<string>("center"),
                        Id = "talker",
                        Content = APLValue.To<string>("${payload.script.properties.text}"),
                        Speech = APLValue.To<string>("${payload.script.properties.speech}")
                    })
                {
                Height = "100vh",
                Width = "100vw",
                AlignItems = "center",
                Direction = "column",
                JustifyContent = new APLValue<string>("center"),
                Grow = new APLValue<int?>(1)
                });
            mainLayout.Parameters = new List<Parameter>();
            mainLayout.Parameters.Add(new Parameter("payload"));

            var speech = new Speech(new PlainText(sentence));
            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = mainLayout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }
                },
                DataSources = new Dictionary<string, APLDataSource>
                {
                    {
                        "script",new ObjectDataSource
                        {
                            Properties = new Dictionary<string, object>
                            {
                                { "ssml", speech.ToXml()}
                            },
                            Transformers = new List<APLTransformer>{
                                APLTransformer.SsmlToText(
                                    "ssml",
                                    "text"),
                                APLTransformer.SsmlToSpeech(
                                    "ssml",
                                    "speech")
                            }
                        }

                    }
                }
            };
            return Task.FromResult(renderDocument);

        }

        public Task<RenderDocumentDirective> BuildDefaultDirective(string sentences, string title = "", string subtitle = "")
        {
            var layout = new Layout
            {
                Description = "Main template",
                Parameters = new List<Parameter> { new Parameter("payload") },
                Items = new Container[]
                {
                    new Container
                    {
                        Height = "100vh",
                         Items=new APLValue<List<APLComponent>>
                         {
                             Value = new List<APLComponent>
                             {
                                 new Image
                                 {
                                    Width = "100vw",
                                    Height = "100vh",
                                    Position = new APLValue<string>("absolute"),
                                    Scale = new APLValue<Scale?>(Scale.BestFill),
                                    Source = new APLValue<string>(BgUrl)
                                 },

                                 new AlexaHeader
                                {
                                    HeaderTitle = new APLValue<string>(title),
                                    HeaderSubtitle = new APLValue<string>(subtitle),
                                    HeaderAttributionImage = new APLValue<string>(LogoUrl)

                                },

                                 new Container
                                 {
                                     Grow = new APLValue<int?>(1),
                                     PaddingLeft = 60,
                                     PaddingRight = 60,
                                     PaddingBottom = 40,
                                     Items = new APLValue<List<APLComponent>>
                                     {
                                         Value = new List<APLComponent>
                                         {

                                             new Text(sentences)
                                             {
                                                FontSize = "30dp",
                                                Style = new APLValue<string>("textStyleBody"),
                                                Spacing = 12
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                    }
                }
            };

            var renderDocument = new RenderDocumentDirective
            {
                Token = "randomToken",
                Document = new APLDocument
                {
                    MainTemplate = layout,
                    Version = APLDocumentVersion.V1_3,
                    Imports = new List<Import>
                    {
                        new Import
                        {
                            Name="alexa-layouts",
                            Version = "1.1.0"
                        }
                    }
                }

            };
            return Task.FromResult(renderDocument);
        }




    }   
     
}
